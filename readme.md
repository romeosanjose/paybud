#Pay Bud
> This is a sample script for getting commission fee.

#### Steps on how to setup to local environment ####
- Clone this application to your specified directory.
- Enter composer install
- this will install all the dependencies needed on your app.
#### How to run the script #####
- on your root project directory enter the following command: php script.php \<csv file path>
- This will run the script getting the commission fee from the excel file "input.csv"
#### How to run unit testing ####
Just enter the command ./vendor/bin/phpunit \<test directory> from your root directory