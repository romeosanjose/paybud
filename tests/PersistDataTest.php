<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 01/08/2019
 * Time: 1:32 PM
 */

namespace Test;


use App\Entity\Transaction;
use App\Repository\TransactionRepositoryImpl;
use PHPUnit\Framework\TestCase;



class PersistDataTest extends TestCase
{
    public function testStoreData()
    {
        session_unset();

        $t1a = new Transaction();
        $t1a->setUserId(0000);
        $t1b = new Transaction();
        $t1b->setUserId(0000);

        $t2a = new Transaction();
        $t2a->setUserId(1111);
        $t2b = new Transaction();
        $t2b->setUserId(1111);

        $tr1Repo = new TransactionRepositoryImpl();
        $tr1Repo->storeByUserId($t1a);
        $tr1Repo->storeByUserId($t1b);

        $result = $tr1Repo->findByUserId(0000);

        $this->assertEquals(2, count($result));

    }
}
