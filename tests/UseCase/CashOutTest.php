<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 8:09 PM
 */

namespace Test\UseCase;


use App\Entity\Transaction;
use App\Repository\TransactionRepositoryImpl;
use App\UseCase\CashOut;
use PHPUnit\Framework\TestCase;


class CashOutTest extends TestCase
{
    /**
     * @dataProvider cashOutDataProvider
     */
    public function testCashOutWithFreePrevious($date, $userId, $userType, $amount, $currency, $expectedComission, $persistedData)
    {
        $currencyClass = 'App\\Entity\\Currency\\' . $currency . 'Currency';
        $currencyObj = new $currencyClass();

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setTransactionDate($date);
        $transaction->setUserId($userId);
        $transaction->setUserType($userType);

        $cashOut = new CashOut($this->getTransactionRepoMock($persistedData));
        $transaction = $cashOut->doCashOut($transaction, $currencyObj);
        $this->assertEquals($expectedComission, $transaction->getConvertedComission());
    }

    public function cashOutDataProvider()
    {
        return [
            [' 2016-01-06',1,'natural',30000,'JPY', 90.00, []],
            ['2016-01-07',1,'natural',1000.00,'EUR', 0.70,
                [
                    [
                        'transaction_date' => '2016-01-06',
                        'user_id' => 1,
                        'user_type' => 'natural',
                        'amount' => 30000,
                        'is_discounted' => false,
                        'converted_amount' => 231.6065776268,
                    ]
                ]
            ],
            ['2016-01-07',1,'natural',100.00,'USD',0.30,
                [
                    [
                        'transaction_date' => '2016-01-06',
                        'user_id' => 1,
                        'user_type' => 'natural',
                        'amount' => 30000,
                        'is_discounted' => false,
                        'converted_amount' => 231.6065776268,
                    ],
                    [
                        'transaction_date' => '2016-01-07',
                        'user_id' => 1,
                        'user_type' => 'natural',
                        'amount' => 1000.00,
                        'is_discounted' => false,
                        'converted_amount' => 1000.00,
                    ]
                ]
            ],
            ['2016-01-10', 1, 'natural', 100.00, 'EUR', 0.30,
                [
                    [
                        'transaction_date' => '2016-01-06',
                        'user_id' => 1,
                        'user_type' => 'natural',
                        'amount' => 30000,
                        'is_discounted' => false,
                        'converted_amount' => 231.6065776268,
                    ],
                    [
                        'transaction_date' => '2016-01-07',
                        'user_id' => 1,
                        'user_type' => 'natural',
                        'amount' => 1000.00,
                        'is_discounted' => false,
                        'converted_amount' => 1000.00,
                    ],
                    [
                        'transaction_date' => '2016-01-07',
                        'user_id' => 1,
                        'user_type' => 'natural',
                        'amount' => 100.00,
                        'is_discounted' => false,
                        'converted_amount' => 100.00,
                    ]
                ]
            ]
        ];
    }

    private function getTransactionRepoMock($persistedData)
    {
        $mockObject = $this->getMockBuilder(TransactionRepositoryImpl::class)
            ->disableOriginalConstructor()
            ->setMethods(['find','store','findByUserId'])
            ->getMock();
        $mockObject->method('findByUserId')
            ->willReturn($persistedData);

        return $mockObject;
    }


    /**
     * @dataProvider dataForCashOutWithAlreadyPreviousDiscount
     */
    public function testCashOutWithAlreadyPreviousDiscount($date, $userId, $userType, $amount, $currency,
                                                           $expectedComission, $expectedDiscounted, $persistedData)
    {
        $currencyClass = 'App\\Entity\\Currency\\' . $currency . 'Currency';
        $currencyObj = new $currencyClass();

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setTransactionDate($date);
        $transaction->setUserId($userId);
        $transaction->setUserType($userType);

        $cashOut = new CashOut($this->getTransactionRepoMock($persistedData));
        $transaction = $cashOut->doCashOut($transaction, $currencyObj);
        $this->assertEquals($expectedComission, $transaction->getConvertedComission());
        $this->assertEquals($expectedDiscounted, $transaction->getisDisCounted());
    }

    public function dataForCashOutWithAlreadyPreviousDiscount()
    {
        return [
            ['2014-12-31',4,'natural',1200,'EUR', 0.60 , true,  []],
            ['2015-01-01',4,'natural',1000.00,'EUR', 3.00, false,
                [
                    [
                        'transaction_date' => '2014-12-31',
                        'user_id' => 4,
                        'user_type' => 'natural',
                        'amount' => 1200,
                        'converted_amount' => 1200,
                        'is_discounted' => true
                    ]
                ]
            ],
        ];
    }

    /**
     * @dataProvider dataForLegalOperation
     */
    public function testCashOutWithLegalOperation($date, $userId, $userType, $amount, $currency, $expectedComission)
    {
        $currencyClass = 'App\\Entity\\Currency\\' . $currency . 'Currency';
        $currencyObj = new $currencyClass();

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setTransactionDate($date);
        $transaction->setUserId($userId);
        $transaction->setUserType($userType);

        $cashOut = new CashOut(new TransactionRepositoryImpl());
        $transaction = $cashOut->doCashOut($transaction, $currencyObj);
        $this->assertEquals($expectedComission, $transaction->getConvertedComission());
    }

    public function dataForLegalOperation()
    {
        return [
            ['2014-12-31',4,'legal',300,'EUR', 0.90],
            ['2014-12-31',4,'legal',100,'EUR', 0.00],
        ];
    }
}
