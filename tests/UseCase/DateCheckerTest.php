<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 31/07/2019
 * Time: 8:13 AM
 */

namespace Test\UseCase;


use App\UseCase\DateChecker;
use PHPUnit\Framework\TestCase;


class DateCheckerTest extends TestCase
{
    public function testGetFirstDateOfWeekFromADate()
    {
        $dateChecker = new DateChecker();
        $firstOfweek = $dateChecker->getFirstOfWeekFromDate('2019-07-31');
        $this->assertEquals(date('Y-m-d', strtotime('2019-07-29')), $firstOfweek);
    }

    public function testGetDatesInWeekOfDate()
    {
        $dateChecker = new DateChecker();
        $datesInWeekOfGivenDate = $dateChecker->getDatesInWeekOfDate('2019-07-31');
        $this->assertEquals(7, count($datesInWeekOfGivenDate));
        $this->assertEquals(date('Y-m-d', strtotime('2019-08-04')), $datesInWeekOfGivenDate[6]);
    }

}