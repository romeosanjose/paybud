<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 9:29 AM
 */

namespace Test\UseCase;

use App\Entity\Transaction;
use App\Repository\TransactionRepositoryImpl;
use App\UseCase\CashIn;
use PHPUnit\Framework\TestCase;


class CashInTest extends TestCase
{
    /**
     * @dataProvider cashInDataProvider
     */
    public function testDoCashIn($date, $userId, $userType, $amount, $currency, $expectedComission)
    {
        $currencyClass = 'App\\Entity\\Currency\\' . $currency . 'Currency';
        $currencyObj = new $currencyClass();

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setTransactionDate($date);
        $transaction->setUserId($userId);
        $transaction->setUserType($userType);


        $cashIn = new CashIn(new TransactionRepositoryImpl());
        $transaction = $cashIn->doCashIn($transaction, $currencyObj);
        $this->assertEquals($expectedComission, $transaction->getConvertedComission());
    }

    public function cashInDataProvider()
    {
        return [
            ['2016-01-10',2,'legal',1000000.00,'EUR', 5.00],
            ['2016-01-05',1,'natural',200.00, 'EUR', 0.06]
        ];
    }
}
