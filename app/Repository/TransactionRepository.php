<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 12:12 PM
 */

namespace App\Repository;


use App\Entity\Transaction;


interface TransactionRepository extends Repository
{
    public function findByUserId($userId);

    public function storeByUserId(Transaction $transaction);
}