<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 11:27 AM
 */

namespace App\Repository;


use App\Entity\Entity;


interface Repository
{
    public function find($id);

    public function store(Entity $entity);
}