<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 11:49 AM
 */
namespace App\Repository;

use App\Entity\Transaction;


class TransactionRepositoryImpl extends AbstractRepository implements TransactionRepository
{
    public function findByUserId($userId)
    {
        if (isset($_SESSION[$userId])) {
            return json_decode($_SESSION[$userId],true);
        } else {
            return null;
        }
    }

    public function storeByUserId(Transaction $transaction)
    {
        $transactionList = [];
        if (isset($_SESSION[$transaction->getUserId()])) {
            $record = json_decode($_SESSION[$transaction->getUserId()],true);
            foreach ($record as $t) {
                array_push($transactionList, $t);
            }
            array_push($transactionList, $transaction->toArray());
        } else {
            array_push($transactionList, $transaction->toArray());
        }

        $_SESSION[$transaction->getUserId()] = json_encode($transactionList);
    }

}