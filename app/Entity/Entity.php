<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 1:01 PM
 */

namespace App\Entity;


abstract class Entity
{
    public function generateSalt($words = [])
    {
        $salt = '';
        foreach($words as $w) {
            $salt = $salt . $w;
        }

        return $salt;
    }

    abstract function toArray();
}