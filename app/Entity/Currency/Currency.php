<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 9:53 AM
 */

namespace App\Entity\Currency;


use App\Entity\Entity;


abstract class Currency extends Entity
{
    abstract public function getCurrencyValue();
    abstract public function getCurrencyCode();

    public function convertToEUR($amount)
    {
        return $amount / $this->getCurrencyValue();
    }

    public function convertToCurrentCurrency($amount)
    {
        return number_format($this->roundUp($amount * $this->getCurrencyValue(),2),2);
    }

    private function roundUp ( $value, $places=0 ) {
        if ($places < 0) {
            $places = 0;
        }
        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }

    function toArray()
    {
        // TODO: Implement toArray() method.
    }


}