<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 12:58 PM
 */

namespace App\Entity\Currency;


class EURCurrency extends Currency
{
    private $currencyAmount = 1;

    public function getCurrencyCode()
    {
        return 'EUR';
    }

    public function getCurrencyValue()
    {
        return $this->currencyAmount;
    }
}