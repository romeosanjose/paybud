<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 11:00 AM
 */

namespace App\Entity\Currency;


class USDCurrency extends Currency
{
    private $currencyAmount = 1.1497;

    public function getCurrencyCode()
    {
        return 'USD';
    }

    public function getCurrencyValue()
    {
        return $this->currencyAmount;
    }
}