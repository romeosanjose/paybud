<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 10:45 AM
 */

namespace App\Entity\Currency;


class JPYCurrency extends Currency
{

    private $currencyAmount = 129.53;

    public function getCurrencyCode()
    {
        return 'JPY';
    }

    public function getCurrencyValue()
    {
        return $this->currencyAmount;
    }

}