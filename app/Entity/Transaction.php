<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 12:50 PM
 */

namespace App\Entity;


class Transaction extends Entity
{
    private $transactionId;

    private $userId;

    private $userType;

    private $transactionDate;

    private $transactionType;

    private $amount;

    private $convertedAmount;

    private $convertedComission;

    private $comission;

    private $currency;

    private $isPreviousFree;

    private $isDisCounted;

    function toArray()
    {
        return [
            'transaction_id' => $this->getTransactionId(),
            'user_id' => $this->getUserId(),
            'user_type' => $this->getUserType(),
            'transaction_date' => $this->getTransactionDate(),
            'transaction_type' => $this->getTransactionType(),
            'amount' => $this->getAmount(),
            'converted_amount' => $this->getConvertedAmount(),
            'comission' => $this->getConvertedComission(),
            'currency' => $this->getCurrency(),
            'is_previous_free' => $this->getisPreviousFree(),
            'is_discounted' => $this->getisDisCounted()
        ];
    }


    /**
     * @return mixed
     */
    public function getisDisCounted()
    {
        return $this->isDisCounted;
    }

    /**
     * @param mixed $isDisCounted
     */
    public function setIsDisCounted($isDisCounted)
    {
        $this->isDisCounted = $isDisCounted;
    }

    /**
     * @return mixed
     */
    public function getisPreviousFree()
    {
        return $this->isPreviousFree;
    }

    /**
     * @param mixed $isPreviousFree
     */
    public function setIsPreviousFree($isPreviousFree)
    {
        $this->isPreviousFree = $isPreviousFree;
    }

    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param mixed $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }



    /**
     * @return mixed
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * @param mixed $transactionDate
     */
    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = $transactionDate;
    }

    /**
     * @return mixed
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param mixed $transactionType
     */
    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getConvertedAmount()
    {
        return $this->convertedAmount;
    }

    /**
     * @param mixed $convertedAmount
     */
    public function setConvertedAmount($convertedAmount)
    {
        $this->convertedAmount = $convertedAmount;
    }

    /**
     * @return mixed
     */
    public function getConvertedComission()
    {
        return $this->convertedComission;
    }

    /**
     * @param mixed $convertedAmount
     */
    public function setConvertedComission($convertedComission)
    {
        $this->convertedComission = $convertedComission;
    }

    /**
     * @return mixed
     */
    public function getComission()
    {
        return $this->comission;
    }

    /**
     * @param mixed $comission
     */
    public function setComission($comission)
    {
        $this->comission = $comission;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }


}