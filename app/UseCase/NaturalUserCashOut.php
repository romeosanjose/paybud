<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 01/08/2019
 * Time: 12:04 PM
 */

namespace App\UseCase;


use App\Entity\Currency\Currency;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;


class NATURALUserCashOut extends CashOutOperation
{
    public function executeCashOut(TransactionRepository $transactionRepository, Transaction $transaction,
                                   Currency $currency, $convertedAmount)
    {
        $persistedTransactions = $transactionRepository->findByUserId($transaction->getUserId());
        $extractedTransactions = $this->extractTransactionsWithinTheWeek($transaction->getTransactionDate(),
            $persistedTransactions);
        if (count($extractedTransactions) <= 2 && count($extractedTransactions) > 0) {
            $transaction = $this->processComission($extractedTransactions, $convertedAmount,  $currency,
                $transaction, $isTotal = true);
        } else {
            $transaction = $this->processComission($extractedTransactions, $convertedAmount,  $currency, $transaction);
        }

        return $transaction;
    }

    private function processComission($extractedTransactions, $convertedAmount, Currency $currency, Transaction $transaction, $isTotal = false)
    {
        if ($this->isAlreadyAcquiredDiscount($extractedTransactions)) {
            $transaction = $this->computeComission($convertedAmount, $convertedAmount, $transaction, $currency);
        } else {
            $transaction = $this->computeComissionWithRules($convertedAmount, $currency, $transaction,
                $extractedTransactions, $isTotal);
        }

        return $transaction;
    }

    private function computeComissionWithRules($convertedAmount, Currency $currency, Transaction $transaction,
                                      $extractedTransactions, $isTotal = false)
    {
        if ($convertedAmount > CashOut::MAX_AMOUNT) {
            $excess = $convertedAmount - CashOut::MAX_AMOUNT;
            return $this->computeComission($excess, $convertedAmount, $transaction, $currency,  true);
        } else if ($convertedAmount === CashOut::MAX_AMOUNT) {
          //  var_dump($extractedTransactions);
            $previousTxn = $this->previousTransaction($extractedTransactions);
            if ($previousTxn && $previousTxn['converted_amount'] < CashOut::MAX_AMOUNT
                && $isTotal) {
                $totalAmount = $this->calculateTotalAmountTransaction($extractedTransactions)  + $convertedAmount;
                if ($totalAmount > CashOut::MAX_AMOUNT) {
                    $excess = $totalAmount - CashOut::MAX_AMOUNT;
                    $transaction = $this->computeComission($excess, $convertedAmount, $transaction, $currency,
                        false, true);
                } else {
                    $transaction = $this->computeComission($totalAmount, $convertedAmount, $transaction, $currency,
                        false, true);
                }
            } else {
                $comission = CashOut::FREE_AMOUNT;
                $convertedComission = $this->convertToCurrentCurrency($comission, $currency);
                $transaction = $this->fillTransaction($transaction, $currency, $comission,
                    $convertedComission, $convertedAmount, true);
            }
        } else {
            $transaction = $this->computeComission($convertedAmount, $convertedAmount, $transaction, $currency);
        }

        return $transaction;
    }

    public function computeComission($amountForComissionCompute, $convertedAmount, Transaction $transaction,
                                     Currency $currency, $isDiscounted = false, $isPreviousFree = false)
    {
        $comission = $this->calculateComission($amountForComissionCompute);
        $convertedComission = $this->convertToCurrentCurrency($comission, $currency);

        return  $this->fillTransaction($transaction, $currency, $comission,
            $convertedComission, $convertedAmount, $isDiscounted, $isPreviousFree);
    }
}