<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 31/07/2019
 * Time: 8:12 AM
 */

namespace App\UseCase;


class DateChecker
{
    public function getFirstOfWeekFromDate($date)
    {
        $dayOfWeek = date('N', strtotime($date)) - 1;
        $convertedDate = strtotime($date);
        $firstOfWeek =  date('Y-m-d', strtotime("- {$dayOfWeek} day", $convertedDate));

        return $firstOfWeek;
    }

    public function getDatesInWeekOfDate($date)
    {
        $firstDayOfWeek = strtotime($this->getFirstOfWeekFromDate($date));
        $datesInWeekOfDate = [];
        for($i = 0; $i < 7; $i++) {
            array_push($datesInWeekOfDate, date('Y-m-d', strtotime("+ {$i} day", $firstDayOfWeek)));
        }

        return $datesInWeekOfDate;
    }
}