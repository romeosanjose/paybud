<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 01/08/2019
 * Time: 12:16 PM
 */

namespace App\UseCase;

use App\Entity\Transaction;
use App\Entity\Currency\Currency;

class CashOutOperation
{

    protected function previousTransaction($extractedTransactions)
    {
        return (is_array($extractedTransactions) && count($extractedTransactions) > 0)
            ? $extractedTransactions[count($extractedTransactions)-1]
            : false;
    }

    protected function isAlreadyAcquiredDiscount($extractedTransactions)
    {
        foreach($extractedTransactions as $et) {
            if ($et['is_discounted'] === true) {
                return true;
            }
        }
        return false;
    }

    protected function fillTransaction(Transaction $transaction, Currency $currency, $comission,
                                       $convertedComission, $convertedAmount, $isDiscounted = false,
                                       $isPreviousFree = false )
    {
        $transaction->setComission($comission);
        $transaction->setCurrency($currency->getCurrencyCode());
        $transaction->setConvertedComission($convertedComission);
        $transaction->setConvertedAmount($convertedAmount);
        $transaction->setIsDisCounted($isDiscounted);
        $transaction->setIsPreviousFree($isPreviousFree);
        $transaction->setTransactionId($transaction->generateSalt(
            [$transaction->getUserId() . '-' . $transaction->getTransactionDate()]
        ));

        return $transaction;
    }



    protected function calculateComission($amount)
    {
        $comission = ($amount * 0.3) / 100;

        return $comission;
    }

    protected function calculateTotalAmountTransaction($extractedTransactions)
    {
        $sum = 0;
        foreach ($extractedTransactions as $et) {
            $sum = $sum + $et['converted_amount'];
        }

        return $sum;
    }

    protected function extractTransactionsWithinTheWeek($date, $persistedTransactions)
    {
        $transactions = [];
        $daysOfWeek = (new DateChecker())->getDatesInWeekOfDate($date);
        if (is_array($persistedTransactions) && count($persistedTransactions) > 0) {
            foreach ($persistedTransactions as $transaction) {
                if (in_array($transaction['transaction_date'], $daysOfWeek)) {
                    $transactions[] = $transaction;
                }
            }
        }
        return $transactions;
    }

    public function convertToEUR($amount, Currency $currency)
    {
        return $amount / $currency->getCurrencyValue();
    }

    public function convertToCurrentCurrency($amount, Currency $currency )
    {
        return number_format($this->roundUp($amount * $currency->getCurrencyValue(),2),2);
    }

    private function roundUp ( $value, $places=0 ) {
        if ($places < 0) {
            $places = 0;
        }
        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }


}