<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 31/07/2019
 * Time: 7:32 AM
 */

namespace App\UseCase;


use App\Entity\Currency\Currency;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;


class CashOut extends CashFlow
{
    private $transactionRepository;

    const MAX_AMOUNT = 1000.00;

    const FREE_AMOUNT = 0.00;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function doCashOut(Transaction $transaction, Currency $currency)
    {
        $convertedAmount = $this->convertToEUR($transaction->getAmount(), $currency);

        $cashOutOperationClass = '\\App\\UseCase\\' . ucfirst(strtolower($transaction->getUserType())) . 'UserCashOut';
        $cashOutOperationObject = new $cashOutOperationClass();

        $transaction = $cashOutOperationObject->executeCashOut($this->transactionRepository, $transaction,
            $currency, $convertedAmount);

        $this->transactionRepository->storeByUserId($transaction);

        return $transaction;
    }


}