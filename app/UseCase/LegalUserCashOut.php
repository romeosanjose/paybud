<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 01/08/2019
 * Time: 1:10 PM
 */

namespace App\UseCase;


use App\Entity\Currency\Currency;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;


class LegalUserCashOut extends CashOutOperation
{
    public function executeCashOut(TransactionRepository $transactionRepository, Transaction $transaction,
                                   Currency $currency, $convertedAmount)
    {
        return $this->computeComission($convertedAmount, $convertedAmount, $transaction, $currency);
    }

    public function computeComission($amountForComissionCompute, $convertedAmount, Transaction $transaction,
                                     Currency $currency, $isDiscounted = false, $isPreviousFree = false)
    {
        $comission = $this->calculateComission($amountForComissionCompute);
        if ($comission < 0.50) {
            $comission = 0.00;
        }

        $convertedComission = $this->convertToCurrentCurrency($comission, $currency);
        return  $this->fillTransaction($transaction, $currency, $comission,
            $convertedComission, $convertedAmount, $isDiscounted, $isPreviousFree);
    }
}