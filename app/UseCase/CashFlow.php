<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 1:28 PM
 */

namespace App\UseCase;


use App\Entity\Currency\Currency;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;


class CashFlow
{
    public function persistTransaction(Transaction $transaction, TransactionRepository $transactionRepository)
    {
        $result = $transactionRepository->store($transaction);
        if (!$result) {
            throw new PersistEntityException("unable to save transaction");
        }
    }

    public function convertToEUR($amount, Currency $currency)
    {
        return $amount / $currency->getCurrencyValue();
    }

    public function convertToCurrentCurrency($amount, Currency $currency )
    {
        return number_format($this->roundUp($amount * $currency->getCurrencyValue(),2),2);
    }

    private function roundUp ( $value, $places=0 ) {
        if ($places < 0) {
            $places = 0;
        }
        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }
}