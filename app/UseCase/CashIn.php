<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 30/07/2019
 * Time: 9:34 AM
 */

namespace App\UseCase;


use App\Entity\Currency\Currency;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;


class CashIn extends CashFlow
{
    private $transactionRepo;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepo = $transactionRepository;
    }

    public function doCashIn(Transaction $transaction, Currency $currency)
    {
        $convertedAmount = $this->convertToEUR($transaction->getAmount(), $currency);
        $comission = $this->calculateComission($convertedAmount);
        $convertedComission = $this->convertToCurrentCurrency($comission, $currency);
        $transaction = $this->fillTransaction($transaction, $currency, $comission, $convertedComission, $convertedAmount);

        return $transaction;
    }

    private function fillTransaction(Transaction $transaction, Currency $currency, $comission,
                                     $convertedComission, $convertedAmount)
    {
        $transaction->setComission($comission);
        $transaction->setCurrency($currency->getCurrencyCode());
        $transaction->setConvertedComission($convertedComission);
        $transaction->setConvertedAmount($convertedAmount);
        $transaction->setTransactionId($transaction->generateSalt(
            [$transaction->getUserId() . '-' . $transaction->getTransactionDate()]
        ));

        return $transaction;
    }

    private function calculateComission($amount)
    {
        $comission = (($amount * 0.03) / 100);

        return ($comission > 5.00)
            ? 5.00
            : $comission;
    }
}