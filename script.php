<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 01/08/2019
 * Time: 1:31 PM
 */
require_once 'vendor/autoload.php';

$redis = new \RedisClient\RedisClient([
    '127.0.0.1:6379',
    'timeout' => 2
]);
$redis->executeRaw(['FLUSHALL']);

$fileHandle = fopen($argv[1], "r");
$csvArray = array();
$transactionList = [];


while ($row = fgetcsv($fileHandle)) {

    switch($row[5]) {
        case 'JPY':
            $currencyObj = new \App\Entity\Currency\JPYCurrency();
            break;
        case 'USD':
            $currencyObj = new \App\Entity\Currency\USDCurrency();
            break;
        case 'EUR':
            $currencyObj = new \App\Entity\Currency\EURCurrency();
            break;
        default:
            throw new \App\Entity\Currency\CurrencyNotFoundException();
    }

    $transaction = new \App\Entity\Transaction();
    $transaction->setAmount($row[4]);
    $transaction->setTransactionDate($row[0]);
    $transaction->setUserId($row[1]);
    $transaction->setUserType($row[2]);


    if ($row[3] == 'cash_out') {
        $cashOut = new \App\UseCase\CashOut(new \App\Repository\TransactionRepositoryImpl());
        $transaction = $cashOut->doCashOut($transaction, $currencyObj);
        if ($transaction->getisPreviousFree()) {
            if (count($transactionList) > 0) {
                $previousTxn = $transactionList[count($transactionList) - 1];
                $previousTxn->setConvertedComission(0);
                $transactionList[count($transactionList) - 1] = $previousTxn;
            }
        }
        array_push($transactionList, $transaction);
    } else {
        $cashIn = new \App\UseCase\CashIn(new \App\Repository\TransactionRepositoryImpl());
        $transaction = $cashIn->doCashIn($transaction, $currencyObj);
        array_push($transactionList, $transaction);
    }

}

foreach($transactionList as $txn) {
    echo "{$txn->getConvertedComission()} \n";
}

fclose($fileHandle);
